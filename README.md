# TYPO3 Extension: CkEditor Addons

**What does it do**
This Extension provides some CkEditor Plugins and Configurations.

- CkEditor Font-Aweseome5 Glyph-Picker

**Installation:**<br />
```composer req typoworx/tnm-ckeditor-addons```

**Activate HTML-Sanitizer for your own TYPO3 site-package**<br />
```php
$GLOBALS['TYPO3_CONF_VARS']['SYS']['features']['security.backend.htmlSanitizeRte'] = true;
$GLOBALS['TYPO3_CONF_VARS']['SYS']['htmlSanitizer'][ $extensionKey ] = \TYPOworx\TnmCkeditorAddons\RTE\HtmlSanitizer\CkEditorGlyphsSanitizer::class;
```
