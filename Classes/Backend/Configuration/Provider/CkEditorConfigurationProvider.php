<?php
declare(strict_types=1);
namespace TYPOworx\CkeditorAddons\Backend\Configuration\Provider;

use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Localization\LanguageService;
use TYPO3\CMS\Form\Mvc\Configuration\ConfigurationManager;
use TYPO3\CMS\Lowlevel\ConfigurationModuleProvider\ProviderInterface;

class CkEditorConfigurationProvider implements ProviderInterface
{
    protected string $identifier;
    protected ConfigurationManager $configurationManager;


    public function __construct(ConfigurationManager $configurationManager)
    {
        $this->configurationManager = $configurationManager;
    }

    public function __invoke(array $attributes): self
    {
        $this->identifier = $attributes['identifier'];

        return $this;
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getLabel(): string
    {
        return $this->getLanguageService()->sL('LLL:EXT:tnm_ckeditor_addons/Resources/Private/Language/locallang.xlf:lowlevel.configuration.module.provider.ckEditorYamlConfiguration');
    }

    public function getConfiguration(): array
    {
        $configurationPresets = [];
        foreach ($GLOBALS[ 'TYPO3_CONF_VARS' ][ 'RTE' ][ 'Presets' ] as $presetName => $path)
        {
            // @see \TYPO3\CMS\Core\Configuration\Richtext
            $runtimeCache = GeneralUtility::makeInstance(CacheManager::class)->getCache('runtime');
            $identifier = 'richtext_' . $presetName;
            $configuration = $runtimeCache->get($identifier);

            if ($configuration === false)
            {
                $yamlFileLoader = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Configuration\Loader\YamlFileLoader::class);
                $configuration = $yamlFileLoader->load($GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets'][$presetName]);

                ArrayUtility::naturalKeySortRecursive($configuration);

                if ($configuration === false)
                {
                    continue;
                }

                $configurationPresets[ $presetName ] = $configuration;

                $runtimeCache->set($identifier, $configuration);
            }
        }

        return $configurationPresets;
    }

    protected function getLanguageService(): LanguageService
    {
        return $GLOBALS['LANG'];
    }
}
