<?php
declare(strict_types=1);
namespace TYPOworx\TnmCkeditorAddons\RTE\HtmlSanitizer;

use TYPO3\CMS\Core\Html\DefaultSanitizerBuilder;
use TYPO3\HtmlSanitizer\Behavior;
use TYPO3\HtmlSanitizer\Builder\BuilderInterface;

class CkEditorGlyphsSanitizer extends DefaultSanitizerBuilder implements BuilderInterface
{
    use CkEditorGlyphsSanitizerTrait
    {
        allowFontawesomeGlyphs as private;
    }

    public function createBehavior(): Behavior
    {
        $behavior = parent::createBehavior();
        $behavior
            ->withName('ifMainmetall')
            ->withTags(...$this->allowFontawesomeGlyphs())
        ;

        return $behavior;
    }
}
