<?php
declare(strict_types=1);
namespace TYPOworx\TnmCkeditorAddons\RTE\HtmlSanitizer;

use TYPO3\HtmlSanitizer\Behavior\Tag;

trait CkEditorGlyphsSanitizerTrait
{
    protected function allowFontawesomeGlyphs(): array
    {
        $tags = [];

        $tags['i'] = (new Tag('i', Tag::PURGE_WITHOUT_ATTRS & Tag::PURGE_WITHOUT_CHILDREN))
            ->addAttrs(...$this->globalAttrs)
        ;

        $tags['em'] = (new Tag('em', Tag::PURGE_WITHOUT_ATTRS & Tag::PURGE_WITHOUT_CHILDREN))
            ->addAttrs(...$this->globalAttrs)
        ;

        return $tags;
    }
}
